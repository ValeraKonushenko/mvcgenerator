#include "MVCGenerator.h"
#pragma warning(disable: 4996)
#include <iostream>

bool MVCGenerator::FileExist(const char * p) {
	std::ifstream file(p);
	if (!file.is_open())return false;
	file.close();
	return true;
}

MVCGenerator::MVCGenerator(std::string path) {
	if (path[path.length() - 1] != '\\' ||
		path[path.length() - 1] != '/')
		path += '/';
	this->path = path;
	this->is_gen_model = true;
	this->is_gen_view = true;
	this->is_gen_controller = true;
	this->folder_m = "";
	this->folder_v = "";
	this->folder_c = "";
	this->is_gen_controller_as_action = false;
}

bool MVCGenerator::GetIsGenControllerAsAction() const {
	return this->is_gen_controller_as_action;
}

void MVCGenerator::SetIsGenControllerAsAction(bool is) {
	this->is_gen_controller_as_action = is;
}

void MVCGenerator::SetModelFolder(std::string folder) {
	if (!FileExist((path + folder).c_str()))
		CreateDirectory((path + folder).c_str(), NULL);

	this->folder_m = folder;
	if (this->folder_m[this->folder_m.length() - 1] != '/' ||
		this->folder_m[this->folder_m.length() - 1] != '\\')
		this->folder_m += '/';
}
void MVCGenerator::SetViewFolder(std::string folder) {
	if (!FileExist((path + folder).c_str()))
		CreateDirectory((path + folder).c_str(), NULL);
	this->folder_v = folder;
	if (this->folder_v[this->folder_v.length() - 1] != '/' ||
		this->folder_v[this->folder_v.length() - 1] != '\\')
		this->folder_v += '/';
}
void MVCGenerator::SetControllerFolder(std::string folder) {
	if (!FileExist((path + folder).c_str()))
		CreateDirectory((path + folder).c_str(), NULL);
	this->folder_c = folder;
	if (this->folder_c[this->folder_c.length() - 1] != '/' ||
		this->folder_c[this->folder_c.length() - 1] != '\\')
		this->folder_c += '/';
}

const std::string & MVCGenerator::GetModelFolder() const {
	return this->folder_m;
}
const std::string & MVCGenerator::GetViewFolder() const {
	return this->folder_v;
}
const std::string & MVCGenerator::GetControllerFolder() const {
	return this->folder_c;
}

void MVCGenerator::SetModelSource(std::string src) {
	if (!FileExist(src.c_str()))
		throw std::exception(("Program can not to get access to the model's source: " + src).c_str());
	this->src_m = src;
}
void MVCGenerator::SetViewSource(std::string src) {
	if (!FileExist(src.c_str()))
		throw std::exception(("Program can not to get access to the view's source: " + src).c_str());
	this->src_v = src;
}
void MVCGenerator::SetControllerSource(std::string src) {
	if (!FileExist(src.c_str()))
		throw std::exception(("Program can not to get access to the controller's source: " + src).c_str());
	this->src_c = src;
}

const std::string & MVCGenerator::GetModelSource() const {
	return this->src_m;
}
const std::string & MVCGenerator::GetViewSource() const {
	return this->src_v;
}
const std::string & MVCGenerator::GetControllerSource() const {
	return this->src_c;
}

void MVCGenerator::SetGenProperties(
	bool is_gen_model,
	bool is_gen_controller,
	bool is_gen_view) {
	this->is_gen_model = is_gen_model;
	this->is_gen_view = is_gen_view;
	this->is_gen_controller = is_gen_controller;
}
bool MVCGenerator::IsGenModel()			const {
	return is_gen_model;
}
bool MVCGenerator::IsGenController()	const {
	return is_gen_controller;
}
bool MVCGenerator::IsGenView()			const {
	return is_gen_view;
}


void MVCGenerator::Generating(std::string title) {
	auto Replace = [](char **src, const char *what, const char *forwhat) {
		char *s = strstr(*src, what);
		if (!s) throw std::exception("Error");
		size_t len[3] = {
			strlen(what),
			strlen(forwhat),
			strlen(*src)
		};
		size_t elen = s - *src;//extern lenght
		char *buff = new char[len[2] - len[0] + len[1] + 1];
		memcpy(buff, *src, elen);
		buff[elen] = 0;
		strcat(buff, forwhat);
		strcat(buff, *src + elen + len[0]);
		delete[] * src;
		*src = buff;
	};
	auto KeepOnlyAlNum = [](std::string &str) {
		for (size_t i = 0; i < str.length(); i++)
			if (!isalnum(str[i]))
				str.erase(i--, 1);
	};
	auto ToLower = [](std::string str) -> std::string {
		for (size_t i = 0; i < str.length(); i++)
			str[i] = tolower(str[i]);
		return str;
	};
	auto UcFirst = [](std::string str) -> std::string {
		str[0] = toupper(str[0]);
		return str;
	};
	auto ToCamelCase = [UcFirst](std::string str) -> std::string {
		int pos;
		while (pos = str.find_first_of(" ")) {
			if (pos == -1)break;
			str.erase(pos, 1);
			str[pos] = toupper(str[pos]);
		}
		UcFirst(str);
		return str;
	};
	auto FromCamelCaseToSnakCase = [](std::string str)-> std::string {
		std::string tmp = "";
		int i = 0;
		if (isupper(str[i]))
			tmp += tolower(str[i++]);
		for (; i < str.length(); i++)
			if (isupper(str[i])) {
				tmp += "_";
				tmp += tolower(str[i]);
			}
			else
				tmp += str[i];
		str = tmp;
		return tmp;
	};
	size_t flen = 0;
	using namespace std;
	char *src_buff_m = nullptr;
	char *src_buff_v = nullptr;
	char *src_buff_c = nullptr;
	fstream file;

	KeepOnlyAlNum(title);

	//MODEL
	if (is_gen_model) {
		file.open(this->src_m, ios::binary | ios::in);
		if (!file.is_open())throw std::exception("File is not exist");
		file.seekg(0, ios::end);
		flen = file.tellg();
		file.seekg(0, ios::beg);
		src_buff_m = new char[flen + 1];
		memset(src_buff_m, 0, flen + 1);
		file.read(static_cast<char*>(src_buff_m), flen);
		file.close();

		std::string _title = title;
		_title = ToCamelCase(_title);
		Replace(&src_buff_m, "%%%%%", _title.c_str());

		std::string new_path = path + folder_m + _title + "Model.php";
		file.open(new_path, ios::binary | ios::out);
		if (!file.is_open())throw std::exception("File is not exist");
		file.write(src_buff_m, strlen(src_buff_m));
		file.close();
	}
	//VIEW
	if (is_gen_view) {
		file.open(this->src_v, ios::binary | ios::in);
		if (!file.is_open())throw std::exception("File is not exist");
		file.seekg(0, ios::end);
		flen = file.tellg();
		file.seekg(0, ios::beg);
		src_buff_v = new char[flen + 1];
		memset(src_buff_v, 0, flen + 1);
		file.read(static_cast<char*>(src_buff_v), flen);
		file.close();

		std::string _title = title;
		_title = ToCamelCase(_title);
		_title = FromCamelCaseToSnakCase(_title);
		Replace(&src_buff_v, "%%%%%", _title.c_str());

		std::string new_path = path + folder_v + _title + ".php";
		file.open(new_path, ios::binary | ios::out);
		if (!file.is_open())throw std::exception("File is not exist");
		file.write(src_buff_v, strlen(src_buff_v));
		file.close();
	}
	//CONTROLLER
	if (is_gen_controller) {
		file.open(this->src_c, ios::binary | ios::in);
		if (!file.is_open())throw std::exception("File is not exist");
		file.seekg(0, ios::end);
		flen = file.tellg();
		file.seekg(0, ios::beg);
		src_buff_c = new char[flen + 1];
		memset(src_buff_c, 0, flen + 1);
		file.read(static_cast<char*>(src_buff_c), flen);
		file.close();

		std::string _title = title;
		std::string _title0 = title;
		_title = ToCamelCase(_title);
		Replace(&src_buff_c, "%%%%%", _title.c_str());
		_title = FromCamelCaseToSnakCase(_title);
		Replace(&src_buff_c, "%%%%%0", _title.c_str());
		//system("cls");
		//std::cout << src_buff_c;
		std::string new_path;

		if (is_gen_controller_as_action) {
			new_path = path + folder_c + "Actions.php";
			file.open(new_path, ios::binary | ios::app);
			if (!file.is_open())throw std::exception("File is not exist");
			file.write(src_buff_c, strlen(src_buff_c));
			file.write("\n\n", 2);
			file.close();
		}
		else {
			new_path = path + folder_c + _title0 + "Ctrl.php";
			file.open(new_path, ios::binary | ios::out);
			if (!file.is_open())throw std::exception("File is not exist");
			file.write(src_buff_c, strlen(src_buff_c));
			file.close();
		}
	}



	delete[] src_buff_m;
	delete[] src_buff_v;
	delete[] src_buff_c;
}
#pragma warning(default: 4996)