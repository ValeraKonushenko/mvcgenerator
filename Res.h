#pragma once
#define FNAME "config.dat"
#define GEN_SERIALIZE_ERROR(p) if (!p) { \
			CreateErrorLog("Serialize error");\
			file.close();\
			delete[] buff;\
			return false; }

#define SET_MAIN_PATH		sflags[0]
#define SET_MODEL_SRC		sflags[1]
#define SET_VIEW_SRC		sflags[2]
#define SET_CONTROLLER_SRC	sflags[3]
#define SET_MODEL_F			sflags[4]
#define SET_VIEW_F			sflags[5]
#define SET_CONTROLLER_F	sflags[6]
#define SET_IS_GEN_CTRL_AS_ACTION bflag

#define ARR_SIZE 1024
char sflags[7][ARR_SIZE];
bool bflag = 0;