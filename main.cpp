#include "MVCGenerator.h"
#include "Res.h"
#include <Windows.h>
#include <iostream>
#include <fstream>
#pragma warning(disable: 4996)
/*
~~~~~~~In the FILE "config.dat" ---====>
0 str: MainPath						char		It's begin of all other paths
1 str: SetModelSource				char		path to the source for a model
2 str: SetViewSource				char		path to the source for a view
3 str: SetControllerSource			char		path to the source for a controller
4 str: SetModelFolder				char		the folder for storing of a data(model)
5 str: SetViewFolder				char		the folder for storing of a data(view)
6 str: SetControllerFolder			char		the folder for storing of a data(controller)
7 str: SetIsGenControllerAsAction	int			if set "1" - it will not create different 
												controllers' file. All data will be to saving in
												one file.
*/


/*~~~~~~~~~~~DECLARATION~~~~~~~~~~~*/
bool Serialize();
void CreateConfig();
void CreateErrorLog(const char *str);
void FGenerator(int argc, const char **argv, MVCGenerator &gen);




int main(int argc, const char **argv) {
	if (!Serialize()) {
		CreateConfig();
		if (!Serialize())return 0;
	}

	MVCGenerator gen(SET_MAIN_PATH);
	try{
		gen.SetModelSource(SET_MODEL_SRC);
		gen.SetViewSource(SET_VIEW_SRC);
		gen.SetControllerSource(SET_CONTROLLER_SRC);
	}
	catch (const std::exception&ex){
		CreateErrorLog(ex.what());
	}
	gen.SetModelFolder(SET_MODEL_F);
	gen.SetViewFolder(SET_VIEW_F);
	gen.SetControllerFolder(SET_CONTROLLER_F);
	gen.SetIsGenControllerAsAction(SET_IS_GEN_CTRL_AS_ACTION);

	//gen.Generating("Bar End Plugs");
	FGenerator(argc, argv, gen);
	//system("pause");
	return 1;
}



/*~~~~~~~~~~~IMPLEMENTATION~~~~~~~~~~~*/
bool Serialize() {
	std::ifstream file;
	file.open(FNAME, std::ios::in);
	if (!file.is_open())return false;
	char *buff = nullptr;
	char str_buff[ARR_SIZE];
	//loading
	size_t flen = 0u;
	file.seekg(0, std::ios::end);
	flen = static_cast<size_t>(file.tellg());
	file.seekg(0, std::ios::beg);
	buff = new char[flen];
	file.read(buff, flen);


	char *ppos = buff;
	//0
	for (size_t j = 0; j < 7; j++)
	{
		memset(str_buff, 0, ARR_SIZE);
		ppos = strchr(ppos, '"') + 1;
		GEN_SERIALIZE_ERROR(ppos)
		for (size_t i = 0; i < ARR_SIZE && *ppos != '"'; i++)
			str_buff[i] = *(ppos++);
		strcpy(sflags[j], str_buff);
		ppos = strchr(ppos, '\n') + 1;
	}
	if (strchr(ppos, '0'))	bflag = false;
	else					bflag = true;

	file.close();
	delete[] buff;
	return true;
}
void CreateConfig() {
	std::fstream file;
	file.open(FNAME, std::ios::out);
	if (!file.is_open()) {
		CreateErrorLog("Config can not be created");
		return;
	}

	file.write("MainPath 			= \"\"\n", 17);
	file.write("SetModelSource 		= \"layout/model.php\"\n", 38);
	file.write("SetViewSource 		= \"layout/view.php\"\n", 36);
	file.write("SetControllerSource = \"layout/controller.php\"\n", 46);
	file.write("SetModelFolder 		= \"Model\"\n", 27);
	file.write("SetViewFolder 		= \"View\"\n", 25);
	file.write("SetControllerFolder = \"Controller\"\n", 35);
	file.write("SetIsGenControllerAsAction = 0\n", 31);
	file.close();
}
void CreateErrorLog(const char *str) {
	std::fstream out;
	out.open("error_log.txt", std::ios::app | std::ios::binary);
	out.write(str, strlen(str));
	out.write("\n", 1);
	out.close();
}
void FGenerator(int argc, const char ** argv, MVCGenerator & gen){
	if (argc > 1) {
		char *fbuff = nullptr,
			*p		= nullptr,
			*sbuff	= new char[ARR_SIZE];
		std::fstream file;
		size_t flen = 0u;

		file.open(argv[1], std::ios::in);
		if (!file.is_open()) {
			CreateErrorLog("Error with opening an incoming file");
			return;
		}
		file.seekg(0, std::ios::end);
		flen = static_cast<size_t>(file.tellg());
		file.seekg(0, std::ios::beg);
		fbuff = new char[flen];
		file.read(fbuff, flen);
		file.close();

		memset(sbuff, 0, ARR_SIZE);
		for (size_t i = 0, j = 0; i < flen-1; i++){
			if (fbuff[i] == '\n') {
				try { gen.Generating(sbuff); }
				catch (const std::exception&ex) {
					CreateErrorLog(ex.what());
				}
				//std::cout << "Write! "<< sbuff <<" \n";
				memset(sbuff, 0, ARR_SIZE);
				j = 0;
				continue;
			}
		//system("pause");
			sbuff[j++] = fbuff[i];
			//std::cout << sbuff << "  j: " << j << "    i: " << i << "\n";
		}
		try { gen.Generating(sbuff); }
		catch (const std::exception&ex) {
			CreateErrorLog(ex.what());
		}
		//std::cout << "Write! " << sbuff << " \n";
		
		delete[]fbuff, sbuff;
	}
}
#pragma warning(default: 4996)