#pragma once
#include <string>
#include <fstream>
#include <Windows.h>
class MVCGenerator {
	int a;
	std::string path;		//main path
	std::string folder_m;	//path to model			from main path
	std::string folder_v;	//path to view			from main path
	std::string folder_c;	//path to controller	from main path

	std::string src_m;
	std::string src_v;
	std::string src_c;

	bool is_gen_model;
	bool is_gen_controller;
	bool is_gen_view;

	bool is_gen_controller_as_action;

	bool FileExist(const char *p);
public:
	MVCGenerator(std::string path = "");

	bool GetIsGenControllerAsAction()const;
	void SetIsGenControllerAsAction(bool is);

	void SetModelFolder(std::string folder);
	void SetViewFolder(std::string folder);
	void SetControllerFolder(std::string folder);
	const std::string& GetModelFolder()		const;
	const std::string& GetViewFolder()		const;
	const std::string& GetControllerFolder()const;

	void SetModelSource(std::string src);
	void SetViewSource(std::string src);
	void SetControllerSource(std::string src);
	const std::string& GetModelSource()		const;
	const std::string& GetViewSource()		const;
	const std::string& GetControllerSource()const;

	void SetGenProperties(
		bool is_gen_model,
		bool is_gen_controller,
		bool is_gen_view);
	bool IsGenModel()		const;
	bool IsGenController()	const;
	bool IsGenView()		const;

	void Generating(std::string title);
};